'use strict';
const bcrypt = require("bcrypt");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const encrypt = (password) => bcrypt.hashSync(password, 10);
    await queryInterface.bulkInsert(
      "UserGames",
      [
        {
          email: "admin@gmail.com",
          username: "admin",
          password: encrypt("admin"),
          createdAt: new Date(),
          updatedAt: new Date(),
          role_id: 1
        },
        {
          email: "bayu@gmail.com",
          username: "bayu",
          password: encrypt("bayu"),
          createdAt: new Date(),
          updatedAt: new Date(),
          role_id: 2
        },
        {
          email: "luky@gmail.com",
          username: "luky",
          password: encrypt("luky"),
          createdAt: new Date(),
          updatedAt: new Date(),
          role_id: 2
        },
      ],
      {}
    );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
