'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const encrypt = (password) => bcrypt.hashSync(password, 10);
     await queryInterface.bulkInsert(
       "UserRoles",
       [
         {
          name: "admin",
           createdAt: new Date(),
           updatedAt: new Date(),
         },
         {
           name: "user",
           createdAt: new Date(),
           updatedAt: new Date(),
         },
       ],
       {}
     );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
