 // lib/passport.js
const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { UserGames } = require('../models')
/* Passport JWT Options */
const options = {
// Untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'Binar123' ,
}

passport.use(new JwtStrategy(options, async (payload, done) => {

    // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
    UserGames.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done(err, false))

}))

module.exports = passport