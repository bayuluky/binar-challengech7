const api = require("express").Router();
const UserApiController = require("../controllers/api/UserApiController");
const userApiController = new UserApiController();

api.get("/users", userApiController.index);
api.post("/users/register", userApiController.create);
api.get("/users/:id", userApiController.show);
api.put("/users/update/:id", userApiController.update);
api.delete("/users/delete/:id", userApiController.delete);

module.exports = api;