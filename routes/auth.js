const router = require('express').Router()
const auth = require('../controllers/AuthController')
const passport = require("../lib/passport-local");
const restrict = require('../middlewares/restrict')

router.get('/register', (req, res) => res.render('register'))
router.post('/register', auth.register)
router.get('/login', (req, res) => res.render('login'))
router.post('/login', passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: true
}))
router.get('/dashboard', restrict, auth.dashboard)
router.post("/logout", restrict, auth.logout);

// router.post('/api/v1/auth/register', auth.register) 
// router.post('/api/v1/auth/login', auth.login)

module.exports = router;