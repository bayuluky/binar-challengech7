const web = require("express").Router();
const FormAddController = require("../controllers/FormAddController");
const formAddController = new FormAddController();
const GameController = require("../controllers/GameController");
const gameController = new GameController();
const LandingPageController = require("../controllers/LandingPageController");
const landingPageController = new LandingPageController();
const UserController = require("../controllers/UserController");
const userController = new UserController();
const RoomController = require("../controllers/RoomController");
const roomController = new RoomController();
const AboutController = require("../controllers/AboutController");
const aboutController = new AboutController();
const restrict = require('../middlewares/restrict')

web.get("/", landingPageController.index);
web.get("/game", gameController.index);
web.get("/formadd", restrict, formAddController.index);
web.get("/room", restrict, roomController.index);
web.get("/about", restrict, aboutController.index);

web.get("/users", restrict, userController.index);
web.post("/users/add", restrict, userController.create);
web.get("/users/:id", restrict, userController.show);
web.post("/users/update/:id", restrict, userController.update);
web.get("/users/delete/:id", restrict, userController.delete);

module.exports = web;