const express = require("express");

class GameController {

	index = (req, res) => {
		res.render("game");
	};
}

module.exports = GameController;