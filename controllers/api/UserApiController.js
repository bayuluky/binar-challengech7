const express = require("express");
let users = require("../../db/user.json");

class UserApiController {

	create = (req, res) => {
		const { username, email, password } = req.body;
        const id = users[users.length - 1].id + 1;
        const user = {
            id,
            username,
            email,
            password,
            role_id
        };

        users.push(user);
        res.status(201).json(user);
	};

    index = (req, res) => {
        res.json(users)
    };

    show = (req, res) => {
        const user = users.find((item) => item.id === +req.params.id);
        if (user) {
            res.status(200).json(user);
        } else {
            res.status(200).send("ID not found");
        }
    };

    update = (req, res) => {
        let user = users.find((item) => item.id === +req.params.id);
        const params = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            role_id: req.body.email,
        };
        user = { ...user, ...params };
        users = users.map((item) => (item.id === user.id ? user : item));
        res.status(200).json(user);
    };

    delete = (req, res) => {
        users = users.filter((item) => item.id !== +req.params.id);
        res.status(200).json({
            message: `Users id ${req.params.id} has been deleted!`,
        });
    };
}

module.exports = UserApiController;
