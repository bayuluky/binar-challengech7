const express = require("express");
const {UserGames,UserGameBiodata} = require('../models');

class UserController {

	create = (req, res) => {
		UserGames.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
        }).then(result => {
            console.log(result);
            UserGameBiodata.create({
                name: req.body.name,
                address: req.body.address,
                user_id: result.id
            }).then(biodata => {
                console.log(biodata);
            })
            console.log("Data has been inserted successfully")
            res.status(200).redirect("/users")
        });
	};

    index = (req, res) => {
        UserGames.findAll({
            include: UserGameBiodata,
        })
        .then((data) => {
            let userrole_ = req.user.dataValues.role_id;
            let username_ = req.user.dataValues.username;
            res.render('users', {
                data,
                userrole : userrole_,
                username : username_,
            });
        })
        .catch((error) => {
            console.log('Something Wrong', error);
        });
    };

    show = (req, res) => {
        UserGames.findOne({
            where: {
                id: req.params.id
            },
            include: UserGameBiodata,
        }).then((user) => {
            let userrole_ = req.user.dataValues.role_id;
            let username_ = req.user.dataValues.username;
            res.render('update', {
                user,
                userrole : userrole_,
                username : username_,
            });
        });
    };

    update = (req, res) => {
        UserGames.update({
            username: req.body.username,
            email: req.body.email,
        }, {
            where: {
                id: req.params.id
            }
        })
        .then(user => {
            // UserGameBiodata.update({
            //     name: req.body.name,
            //     address: req.body.address
            // }, {
            //     where: {
            //         user_id: req.params.id
            //     }
            // })
            // .then(result => {
                console.log(user)
                console.log("Data has been updated successfully")
                res.status(200).redirect("/users")
            // })
        })
    };

    delete = (req, res) => {
        UserGames.destroy({
            where: {
                id: req.params.id
            }
        }).then(
            () => {
                console.log("Data has been deleted successfully")
                res.status(200).redirect("/users")
            }
        );
    };
}

module.exports = UserController;
