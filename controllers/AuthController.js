const {UserGames} = require('../models');
const passport = require('../lib/passport-local' )

function format(user) {
    const { id, username } = user 
    return {
        id,
        username,
        accessToken: user.generateToken()
    } 
}

module.exports = {
    register: (req, res, next) => {
        UserGames.register(req.body)
        .then(() => {
            res.redirect("/login")
        }).catch(err => next(err))
    },
    dashboard: (req, res) => {
        console.log(req.user)
        let userrole_ = req.user.dataValues.role_id;
		let username_ = req.user.dataValues.username;
		res.render('dashboard', {
			userrole : userrole_,
			username : username_,
		});
        // const currentUser = req.user;
        // res.json(currentUser)
    },
    logout: (req, res, next) => {
        req.logout((err) => {
            if (err) next(err)
            res.redirect("/login")
        })
    },
    login: (req, res) => {
        console.log(req.body)
        UserGames.authenticate(req.body) 
            .then(user => {
                res.json(format(user)) 
            })
    },
}