const express = require("express");

class RoomController {

	index = (req, res) => {
        let userrole_ = req.user.dataValues.role_id;
		let username_ = req.user.dataValues.username;
		res.render('room', {
			userrole : userrole_,
			username : username_,
		});
	};
}

module.exports = RoomController;
