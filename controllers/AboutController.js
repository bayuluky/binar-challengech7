const express = require("express");

class AboutController {

	index = (req, res) => {
        let userrole_ = req.user.dataValues.role_id;
		let username_ = req.user.dataValues.username;
		res.render('about', {
			userrole : userrole_,
			username : username_,
		});
	};
}

module.exports = AboutController;
