const express = require("express");

class LandingPageController {

	index = (req, res) => {
		res.render("landingpage");
	};
}

module.exports = LandingPageController;
