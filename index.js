const express = require("express");
const app = express();
const session = require('express-session')
const flash = require('express-flash')
const port = 5050;

app.use(express.urlencoded({ extended: false }));

app.use(session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false
}))

// const passport = require('./lib/passport-jwt')
const passport = require('./lib/passport-local')
app.use(passport.initialize())
app.use(passport.session()) 
app.use(flash())
app.use(express.json());
app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");

const web = require("./routes/web");
app.use("/",web);

const api = require("./routes/api");
app.use("/api",api);

const auth = require('./routes/auth')
app.use(auth)

app.use((req, res, next) => {
    res.status(404).render("notfound");
});

app.listen(port, () => console.log(`Server berjalan di port ${port}`));